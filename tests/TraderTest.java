import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TraderTest {
    // create trade
//    create trader, account
    Trade trade = new Trade("qw", "qwe", 123, 123);
    Account account = new Account();
    Trader trader = new Trader("Joanna", account);

    @Test
    public void testAddTradeArithmetic(){
        trader.addTrade(trade);
        assertEquals(trade.getPrice()*trade.getQuantity(), account.getTotalTrades());
    }
}
