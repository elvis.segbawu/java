public class Account {
    private double totalTrades;

    public Account(){
        totalTrades = 0.0;
    }

    public double getTotalTrades(){
        return  this.totalTrades;
    }

    public void setTotalTrades(double totalTrades){
        this.totalTrades = totalTrades;
    }


}
