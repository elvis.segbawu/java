public class Trader {

    String name;
    Account account;

    public Trader(String name, Account account){
        this.name = name;
        this.account = account;
    }

    public void addTrade(Trade trade){
        account.setTotalTrades(
                account.getTotalTrades()+(trade.getPrice() * trade.getQuantity())
        );
    }

}
