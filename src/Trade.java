public class Trade {

    private String id, symbol;
    private int quantity;
    private double price;





    public Trade(String id, String symbol){
        this(id, symbol, 3);
    }

    public Trade(String id, String symbol, int quantity){
        this(id, symbol, quantity, 54);
    }

    public Trade(String id, String symbol, int quantity, double price){
        this.id = id;
        this.symbol = symbol;
        this.quantity = quantity;
        this.price = price;
    }


    public void setPrice(double price){
        if(price >= 0){
            this.price = price;
        } else {
            System.out.println("negative price rejected");
        }

    }

    @Override
    public String toString(){
        return "id="+id + "\n" + "symbol="+symbol;
    }

    public double getPrice(){
        return this.price;
    }

    public int getQuantity(){
        return this.quantity;
    }

}
